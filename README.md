# OpenML dataset: AirBNB-analysis-Lisbon

https://www.openml.org/d/43599

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Dataset is from http://tomslee.net/airbnb-data-collection-get-the-data 
room_id: A unique number identifying an Airbnb listing. The listing has a URL on the Airbnb web site of http://airbnb.com/rooms/room_id
host_id: A unique number identifying an Airbnb host. The hosts page has a URL on the Airbnb web site of http://airbnb.com/users/show/host_id
room_type: One of Entire home/apt, Private room, or Shared room
borough: A subregion of the city or search area for which the survey is carried out. The borough is taken from a shapefile of the 
city that is obtained independently of the Airbnb web site. For some cities, there is no borough information; for others the borough may be a number. If you have better shapefiles for a city of interest, please send them to me.
neighborhood: As with borough: a subregion of the city or search area for which the survey is carried out. For cities that have both, a neighbourhood is smaller than a borough. For some cities there is no neighbourhood information.
reviews: The number of reviews that a listing has received. Airbnb has said that 70 of visits end up with a review, so the number of reviews can be used to estimate the number of visits. Note that such an estimate will not be reliable for an individual 
listing (especially as reviews occasionally vanish from the site), but over a city as a whole it should be a useful metric of traffic.
overall_satisfaction: The average rating (out of five) that the listing has received from those visitors who left a review.
accommodates: The number of guests a listing can accommodate.
bedrooms: The number of bedrooms a listing offers.
price: The price (in US) for a night stay. In early surveys, there may be some values that were recorded by month.
minstay: The minimum stay for a visit, as posted by the host.
latitude and longitude: The latitude and longitude of the listing as posted on the Airbnb site: this may be off by a few hundred metres. I do not have a way to track individual listing locations with
last_modified: the date and time that the values were read from the Airbnb web site.
The first line of the CSV file holds the column headings.
Here are the cities, the survey dates, and a link to download each zip file.
Aarhus
Survey dates: 2016-10-28 (2258 listings), 2016-11-26 (1900 listings), 2017-01-21 (2167 listings), 2017-02-21 (2295 listings), 2017-03-30 (2323 listings), 2017-04-18 (2398 listings), 2017-04-28 (2360 listings), 2017-05-15 (2437 listings), 2017-06-19 (2802 listings), 2017-07-28 (3142 listings)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43599) of an [OpenML dataset](https://www.openml.org/d/43599). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43599/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43599/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43599/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

